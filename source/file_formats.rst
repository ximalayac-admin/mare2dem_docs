.. _section_file_formats:

File Formats
============

There are five files required to run MARE2DEM: 

    -  :Ref:`sect_resistivity_file` 
    -  :Ref:`sect_model_file` 
    -  :Ref:`sect_penalty_file`  
    -  :Ref:`sect_data_file`
    -  :Ref:`sect_settings_file`

The first two files describe the model geometry, resistivity and fixed
and free inversion parameters (as well as other inversion settings).  
The penalty file stores the penalty matrix. These three files are all
created automatically by ``Mamba2D.m``, so the material below isn't required
reading.  The data file obviously contains the data, and the settings
file contains settings for the parallel data decomposition and adaptive
finite element settings for MARE2DEM.  

.. Note::
   Any text to the right of a ``!`` character in the input files is 
   considered a comment and will be ignore by MARE2DEM.
 
.. admonition:: Developer Note

    In addition to the Fortran
    subroutines used to read and write the MARE2DEM required files, there are  MATLAB
    versions in the folder ``a_util`` in MARE2DEM's MATLAB code distribution:
    
    - ``m2d_readEMData2DFile.m``
    - ``m2d_readPoly.m``
    - ``m2d_readResistivity.m``
    - ``m2d_writeEMData2DFile.m``
    - ``m2d_writePoly.m``
    - ``m2d_writeResistivity.m``

.. toctree::
   :maxdepth: 2
   :hidden:
   
   resistivity_model_format
   penalty_file_format
   data_file_format
   settings_file_format
 



 

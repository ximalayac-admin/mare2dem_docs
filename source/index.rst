..  image:: _static/images/snowmesh.png
    :width: 100%
    :align: center
     
MARE2DEM: Modeling with Adaptively Refined Elements for 2D Electromagnetics
===========================================================================

 

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: User Manual

    about
    overview
    examples 
    install
    download
    geometry
    file_formats
    command_line_args
    scratch
    colormaps
    tips
    theory
    references  

..  to do:
    changelog

 


   
 


 
 
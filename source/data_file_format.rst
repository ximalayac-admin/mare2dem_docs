.. _sect_data_file: 

Data File
---------

The data file lists the transmitters, receivers and frequencies, and has
a table of data parameters, values and standard errors.  The current
data file format is  named  EMData_2.2. This format is specific to the
MARE2DEM inversion code. The Data file can include arbitrary comment
lines (using ``!`` or ``%``) or blank lines. Comments can also be placed
at the end of a given line using ``!`` or ``%`` symbols before the
comment text. All positions are in units of meters and angles are in
degrees.  Here's an example (note that ``...`` indicates where some
lines have been omitted for brevity):
 
.. code-block:: none

    Format:  EMData_2.2
    ! some comment text
    UTM of x,y origin (UTM zone, N, E, 2D strike): 11 N 3636717.0 476297.0   20.0
    Phase Convention: lag
    Reciprocity Used: no
    # CSEM Frequencies:    3
             0.1
             0.3
             0.5    
    # Transmitters:      15
    !      X            Y            Z Azimuth     Dip  Length    Type  Name 
        0.00         0.00      2189.90   90.00   -1.20    0.00  edipole TX01
        0.00       500.00      2181.00   90.00    1.00    0.00  edipole TX02
        0.00      1000.00      2172.10   90.00   -0.80    0.00  edipole TX03
        0.00      1500.00      2163.70   90.00   -0.70    0.00  edipole TX04
    ...
    # CSEM Receivers:   100
    !      X            Y            Z   Theta   Alpha    Beta  Length  Name
        0.00         0.00      2140.00    0.00    0.00    0.00    0.00  RX01
        0.00       100.00      2138.20    0.00    0.00    0.00    0.00  RX02
        0.00       200.00      2137.20    0.00    0.00    0.00    0.00  RX03
        0.00       300.00      2135.70    0.00    0.00    0.00    0.00  RX04
    ...
    # MT Frequencies:    21
          0.0001
        0.000158
        0.000251
    ...
    # MT Receivers:      15
    !      X            Y            Z   Theta   Alpha    Beta   Length SolveStatic    Name
        0.00         0.00      2140.00    0.00    0.00    0.00    0.00          0      RX01
        0.00       100.00      2138.20    0.00    0.00    0.00    0.00          0      RX02
        0.00       200.00      2137.20    0.00    0.00    0.00    0.00          0      RX03
        0.00       300.00      2135.70    0.00    0.00    0.00    0.00          0      RX04
    ...
    # Data:       5596
    !         Type        Freq#        Tx#           Rx#       Data 	  Std_Error
               3            1            1            1  6.42506e-13   7.5608e-14 
               4            1            1            1  1.20096e-12   7.5608e-14 
               3            2            1            1  4.86059e-14  5.08595e-14 
               4            2            1            1  8.69775e-13  5.08595e-14 
               3            3            1            1 -4.24273e-13  3.82199e-14 
    ...
             103            1            1            1      29.4792      1.67651 
             104            1            1            1      26.1554      3.43775 
             105            1            1            1      32.9502      2.12072       
             106            1            1            1      24.4939      3.43775 
    ...
 
The file consists of *token : value* blocks where the *token* is a
keyword or keywords. These are followed by a single *value* or multiple
lines of values. Many of the tokens can appear in any order and the
individual CSEM and MT sections only need to be specified when CSEM or
MT data (or both) are being modeled, respectively. Details of each
section are given below. 
 
UTM of x,y origin
~~~~~~~~~~~~~~~~~

This block is not used by the MARE2DEM code, but is included so that
plotting routines can convert the local 2D coordinate system used for
the data and 2D model into geographical UTM or lat/lon coordinates. See
the :ref:`section_utm0` figure.

You can set these values to 0 if you don't need this.  In this example:

.. code-block:: none

   UTM of x,y origin (UTM zone, N, E, 2D strike): 11 N 3636717.0 476297.0   20.0
 
the UTM origin is set to Scripps Institution of Oceanography and the 2D
strike is at 20 degrees (clockwise from North). This means that the
local 2D modeling coordinates corresponds to geographic coordinates
where x is aligned along 20 degrees and y points along 110 degrees (so 
the 2D model conductivity strike is at 20 degrees, the 2D model profile
runs along the angle 110 degrees from geographic North). Similarly, a
receiver with a given theta angle of 0 degrees in the 2D coordinate
system then corresponds to an angle of 20 degrees from geographic North.
  
Phase Convention  
~~~~~~~~~~~~~~~~

The default convention for MARE2DEM is phase lag (i.e., phases become
increasingly positive with source-receiver offset), but you can instead
specify that the data use a phase lead convention (i.e., phases become
increasingly negative with source-receiver offset):

.. code-block:: none

    Phase Convention: lag         ! Optional, use lag (default) or lead 

Note that the phase convention is ignored by MT data, and TM mode MT
data are expected to have phases wrapped to the first quadrant.  If in
doubt, run a forward model of a half space to see what MARE2DEM outputs
for a given data type.

Reciprocity Used
~~~~~~~~~~~~~~~~

This setting is only used by the MARE2DEM code to handle a source scaling
factor when EM reciprocity has been applied to convert electric sources
and magnetic receivers into magnetic sources and electric receivers. In
all other instances (electric-electric or magnetic-magnetic
reciprocity), MARE2DEM ignores this setting. It is also included in the
data file so that plotting routines have the option to interchange
receivers and transmitters when reciprocity has been applied to ease
computational requirements (when there are significantly more
transmitters than receivers).

Transmitters
~~~~~~~~~~~~

This block lists the number of transmitters and each transmitters's
*x,y,z* (meters) location, horizontal rotation angle (degrees clockwise
from x),  dip angle (degrees positive down), dipole length,  transmitter
type and optionally the transmitter name (used for plotting purposes
only). 

Transmitter Type
================
Possible values:

- ``edipole`` - electric dipole (point or finite length)
- ``bdipole`` -  point magnetic dipoles  

.. _sect_dipole_length:

Dipole length
=============
The length setting only applies to electric dipoles. Generally, the
dipole length should be set to 0 so that MARE2DEM uses a point dipole
approximation (which is computationally efficient). This is recommended
for both the transmitter and receiver dipoles. However, in some CSEM
applications the finite length of the transmitter (or receiver) dipole
is too long to be well approximated by a point dipole and a finite
length wire needs to be modeled (e.g., when the transmitter and receiver
are close to within a few dipole lengths). **Non-zero dipole lengths
should be used with care** as that increases the computational burden on
MARE2DEM and more importantly it can be easily misconfigured with
respect to topography or other surfaces.

For non-zero electric dipole lengths, MARE2DEM  uses a low order
Gauss-Legendre quadrature rule to integrate the source current along the
dipole  wire from *-length/2* to *+length/2* about  the center point
specified by the *x,y,z* location and along the vector defined by the
azimuth and dip. A similar line integral is applied to the electric
field along any non-zero length receiver dipoles. See the
:ref:`sect_settings_file` documentation for how to adjust the quadrature
order.

For CSEM modeling, MARE2DEM assumes a unit current for all sources and
normalizes all responses by the transmitter dipole length (if non-zero,
otherwise a unit dipole moment is used), so for inversions, the input
data should be normalized by the transmitter dipole moment (i.e.,
divided by Am or Am\ :math:`^2` for electric and magnetic dipoles,
respectively). CSEM electric field responses are also normalized by any
non-zero receiver dipole lengths. 

.. Warning:: 

    Non-zero dipole lengths can require significantly more computational
    effort by MARE2DEM and thus should only be used when necessary, for
    example when the receivers are located within a few dipole lengths
    of the transmitter or when better than ~1% accuracy is needed at far
    offsets.

    Further, when using non-zero length dipoles, it is up to the user to
    ensure this is sensible with respect to the model structure, as
    MARE2DEM does not check if finite length dipoles extend across
    boundaries in the model structure or do other potentially
    problematic things with respect to the model struture. For marine
    CSEM this is usually okay since the transmitter is in the water
    column, but for seafloor or land surface transmitters it is super
    easy to mistakenly put a finite length transmitter on the surface
    and not realize that one end of it extends into the air where there
    is topography.

    Stick to point dipoles (0 length) unless you know what you are
    doing. You have been warned. 
 
    
CSEM Frequencies
~~~~~~~~~~~~~~~~

This block lists the number of CSEM frequencies and the specific values
(Hz). If there are no CSEM data, all the CSEM blocks can be omitted from
the data file.

CSEM Receivers
~~~~~~~~~~~~~~

This block lists the number of CSEM receivers each receiver's *x,y,z*
(meters) position,  rotation angles, electric dipole length, and
optionally the receiver name (used for plotting purposes only).  

Position
===============

.. admonition:: Recommendation

    MARE2DEM works best when the  *x* coordinates of the transmitters
    and receivers are within a few 100 meters of the origin. Receivers
    far from the transmitter in the *x* direction can be problematic or
    may require significantly denser wavenumber sampling than the
    MARE2DEM default value. See  :Ref:`alongstrike_warning` for more
    details.
    
Rotation Angles
===============

Theta corresponds to the angle from the 2D modeling coordinate *x* to
the receiver's *x* channel. Alpha and Beta are dip angles. Alpha is the
tilt angle of the *x* channel, positive down from horizontal. Beta is
the angle of the receiver's *y* channel from horizontal. See
:ref:`sect_rx_geometry` for more details.

For normal inline marine CSEM data with transmitters and seafloor EM
receivers nominally along the *y* axis, usually only the Beta angle is
needed and it can be set to the slope of the *modeled* seafloor (so that
the receiver's :math:`E_y` electric field is parallel to the seafloor).

Dipole length
=============
See :Ref:`sect_dipole_length`. 

MT Frequencies
~~~~~~~~~~~~~~

This block lists the number of MT frequencies the specific values (Hz). 
If there are no MT data, all the MT blocks can be omitted from the data
file.

MT Receivers
~~~~~~~~~~~~~~

This block lists the number of MT receivers and each receiver's *x,y,z*
(meters) position,  rotation angles, electric dipole length, static
shift solver flag, and optionally the MT receiver name (used for
plotting purposes only).  

    
Rotation Angles
===============

Theta corresponds to the angle from the 2D modeling
coordinate *x* to the receiver's *x* channel. Alpha and Beta are dip
angles. Alpha is the tilt angle of the *x* channel, positive down from
horizontal. Beta is the angle of the receiver's *y* channel from
horizontal. See :ref:`sect_rx_geometry` for more details.  

For normal MT stations, the MT impedance tensor should be rotated
so that the receiver *x* direction is along the 2D model strike so that 
the Theta angle can be set to zero in MARE2DEM.  The Alpha angle 
should always be set to zero for MT stations.

.. admonition:: Recommendation for seafloor MT

    For seafloor MT stations that nominally have *y* component 
    magnetic and electric 
    dipoles parallel to the seafloor slope, the Beta angle should
    be set to the *modeled* seafloor topography slope.  See
    :Ref:`sect_rx_geometry`.

    
.. admonition:: Recommendation for land MT

    For land MT stations in regions with significant topography,
    usually the electric dipole is parallel to the slope while 
    the magnetometers are installed horizontally. To model this in 
    MARE2DEM, you will need to create a hybrid MT station where
    each real station is defined by two modeled stations: one with
    zero tilt angles for the magnetic fields and the second with
    a non-zero Beta set to the *modeled* topography slope.  See
    :Ref:`sect_rx_geometry` and :Ref:`sect_mt_tilted_electrics` for 
    more details.

Dipole length
=============
The length setting only applies to electric dipoles. Generally
MT receivers should have dipole length  set to 0, unless you 
have are trying to model something highly atypical for normal MT 
applications. For more details, see :Ref:`sect_dipole_length`. 


Static Shift Solver
===================

The *SolveStatic* column for the MT receivers allows for a simple iterative
estimate of MT static shifts during inversion of MT data. Options are:

 - ``0`` - no static shift solution
 - ``1`` - static shift solution for both TE and TM modes
 - ``2`` - static shift solution for only the TE mode
 - ``3`` - static shift solution for only the TM mode
 
This should be used sparingly and usually should be set to 0 unless you
have good reason to suspect a static shift at a particular station. When
enabled, MARE2DEM simply estimates the static shift for each mode as the
frequency-averaged residual of the observed and modeled apparent
resistivity. This requires that the input data be formatted as either
apparent resistivity or its log10 equivalent. This approach to
estimating the static shift works best when only a single station or a
small subset of stations are suspected of having static shifts. The
resulting static shift solutions are shown for each iteration in the
Occam log file.  Synthetic tests show that this method works well for
estimating static shifts, however, those same tests show that for truly
non-static shifted data, it can still give estimated shifts of up to
10-50\%, so us this option only when necessary.

Data Block
~~~~~~~~~~

The Data block lists the number of data and has a table with a 
line for each datum. Each line lists the data parameters, the datum and 
its standard error.  The first four values are data parameters for each 
datum and the 5th and 6th columns are the data and standard errors.

Data Type (1st column)
======================

The first column specifies the data type.  The currently supported data
types for CSEM and MT data are given in tables  :Ref:`sect_csem_types`
and :Ref:`sect_mt_types`  Note that for each receiver,
*x,y,z* components are relative to the local receiver coordinate frame defined by
its given theta, alpha and beta angles. 

Frequency Index  (2nd column)
=============================
This index refers to either the CSEM or MT frequency for this datum,
depending on the data type specified.

Transmitter Index  (3rd column)
===============================

Receiver Index  (4th column)
============================
The receiver index refers to
either an MT or CSEM receiver, again depending on the data type specified.

.. admonition:: Recommendation for hybrid MT stations

    For MT data the transmitter index column is ignored if it is equal to 0, 
    otherwise
    the transmitter index is used to specify which receiver should be used
    for the magnetic fields of the MT response, and the receiver index
    specifies which receiver to use for the electric fields. In this way,
    hybrid MT stations can be modeled (i.e., magnetic fields from one
    receiver and electric fields from another receiver). This can
    also be used for modeling MT stations with horizontal magnetics
    and slope parallel electric fields, by defining two receivers at 
    the same *x,y,z* location with one having tilt angles set to zero
    and the other with non-zero Beta angle.

Data and Uncertainty  (5th & 6th columns)
=========================================
The fifth and sixth columns are the data and standard errors. Standard
errors should be given in the same absolute units as the data (i.e. this
is absolute uncertainty not relative).  See the :Ref:`sect_uncertainty`
section for the details.

Data Units
==========

CSEM fields output from MARE2DEM are normalized to unit dipole
moments so that they are independent of the source moment, and thus
input data to be inverted should be correspondingly normalized by the
source dipoles moment (:math:`length \times current` for electric
dipoles and :math:`area \times current` for magnetic loops). All
phase data use units of degrees.  The table below shows the data
units used in MARE2DEM. 


    +-----------------+-----------------------+-----------------------+
    | Source Type     | Data Type             | Units                 |
    +=================+=======================+=======================+  
    | electric dipole | electric field (E)    |   V/Am\ :math:`^{2}`  |
    +-----------------+-----------------------+-----------------------+
    |                 | magnetic field  (B)   |   T/Am                |
    +-----------------+-----------------------+-----------------------+
    | magnetic dipole | electric field  (E)   |   V/Am\ :math:`^{3}`  |
    +-----------------+-----------------------+-----------------------+
    |                 | magnetic field  (B)   |   T/Am\ :math:`^{2}`  |
    +-----------------+-----------------------+-----------------------+    
    | MT              | apparent resistivity  |   ohm-m               |
    +-----------------+-----------------------+-----------------------+
    |                 | impedance             |   ohm                 |
    +-----------------+-----------------------+-----------------------+
    |                 | tipper                |   unitless            |
    +-----------------+-----------------------+-----------------------+    
    |                 | electric field  (E)   |   V/m                 |
    +-----------------+-----------------------+-----------------------+ 
    |                 | magnetic field  (H)   |   A/m                 |        
    +-----------------+-----------------------+-----------------------+ 
           
.. admonition ::  TM mode MT data phase      

    MARE2DEM currently assumes that the TM mode (:math:`Z_{yx}`) impedance phases 
    have been moved from their nominal value of -135 degrees (for a halfspace)  
    to the first quadrant by adding 180 degrees, so they are nominally 
    45 degrees and thus will plot in the same quadrant as the TE mode phases. 
 
.. _sect_csem_types:
 
CSEM Data types
=============== 

    +-------+-----------------------------------------+
    | Code  | Description                             |
    +=======+=========================================+  
    |  1    |  real Ex                                |
    +-------+-----------------------------------------+ 
    |  2    |  imaginary Ex                           |
    +-------+-----------------------------------------+ 
    |  3    |  real Ey                                |
    +-------+-----------------------------------------+ 
    |  4    |  imaginary Ey                           |
    +-------+-----------------------------------------+ 
    |  5    |  real Ez                                |
    +-------+-----------------------------------------+ 
    |  6    |  imaginary Ez                           |
    +-------+-----------------------------------------+ 
    |  11   |  real Bx                                |
    +-------+-----------------------------------------+ 
    |  12   |  imaginary Bx                           |
    +-------+-----------------------------------------+ 
    |  13   |  real By                                |
    +-------+-----------------------------------------+ 
    |  14   |  imaginary By                           |
    +-------+-----------------------------------------+ 
    |  15   |  real Bz                                |
    +-------+-----------------------------------------+ 
    |  16   |  imaginary Bz                           |
    +-------+-----------------------------------------+ 
    |  21   |  amplitude Ex                           |
    +-------+-----------------------------------------+ 
    |  22   |  phase Ex                               |
    +-------+-----------------------------------------+ 
    |  23   |  amplitude Ey                           |
    +-------+-----------------------------------------+ 
    |  24   |  phase Ey                               |
    +-------+-----------------------------------------+                 
    |  25   |  amplitude Ez                           |
    +-------+-----------------------------------------+ 
    |  26   |  phase Ez                               |
    +-------+-----------------------------------------+              
    |  27   |  log10 amplitude Ex                     |
    +-------+-----------------------------------------+                 
    |  28   |  log10 amplitude Ey                     |
    +-------+-----------------------------------------+ 
    |  29   |  log10 amplitude Ez                     |
    +-------+-----------------------------------------+   
    |  31   |  amplitude Bx                           |
    +-------+-----------------------------------------+ 
    |  32   |  phase Bx                               |
    +-------+-----------------------------------------+ 
    |  33   |  amplitude By                           |
    +-------+-----------------------------------------+ 
    |  34   |  phase By                               |
    +-------+-----------------------------------------+                 
    |  35   |  amplitude Bz                           |
    +-------+-----------------------------------------+ 
    |  36   |  phase Bz                               |
    +-------+-----------------------------------------+              
    |  37   |  log10 amplitude Bx                     |
    +-------+-----------------------------------------+                 
    |  38   |  log10 amplitude By                     |
    +-------+-----------------------------------------+ 
    |  39   |  log10 amplitude Bz                     |
    +-------+-----------------------------------------+
    |  41   | electric xy polarization ellipse max    |
    +-------+-----------------------------------------+ 
    |  42   | electric xy polarization ellipse min    |
    +-------+-----------------------------------------+ 
    |  43   | magnetic xy polarization ellipse max    |
    +-------+-----------------------------------------+ 
    |  44   | magnetic xy polarization ellipse min    |
    +-------+-----------------------------------------+  
 

.. _sect_mt_types:

MT Data types
=============  

    +-------+-----------------------------------------+
    | Code  | Description                             |
    +=======+=========================================+  
    |  103  |TE :math:`Z_{xy}` apparent resistivity   |
    +-------+-----------------------------------------+ 
    |  104  |TE :math:`Z_{xy}` phase                  |
    +-------+-----------------------------------------+ 
    |  105  |TM :math:`Z_{yx}` apparent resistivity   |
    +-------+-----------------------------------------+ 
    |  106  |TM :math:`Z_{yx}` phase                  |
    +-------+-----------------------------------------+ 
    |  123  |TE :math:`Z_{xy}` log10 app. resist.     |
    +-------+-----------------------------------------+ 
    |  125  |TM :math:`Z_{yx}` log10 app. resist.     |
    +-------+-----------------------------------------+ 
    |  113  |TE :math:`Z_{xy}` real                   |
    +-------+-----------------------------------------+ 
    |  114  |TE :math:`Z_{xy}` imaginary              |
    +-------+-----------------------------------------+ 
    |  115  |TM :math:`Z_{yx}` real                   |
    +-------+-----------------------------------------+ 
    |  116  |TM :math:`Z_{yx}` imaginary              |
    +-------+-----------------------------------------+ 
    |  133  |TE :math:`M_{zy}` real tipper            |
    +-------+-----------------------------------------+ 
    |  134  |TE :math:`M_{zy}` imaginary tipper       |
    +-------+-----------------------------------------+ 
    |  135  |TE :math:`M_{zy}` amplitude tipper       |
    +-------+-----------------------------------------+ 
    |  136  |TE :math:`M_{zy}` phase tipper           |
    +-------+-----------------------------------------+ 
    |  151  |  TE mode real Ex                        |
    +-------+-----------------------------------------+ 
    |  152  |  TE mode imaginary Ex                   |
    +-------+-----------------------------------------+ 
    |  153  |  TM mode real Ey                        |
    +-------+-----------------------------------------+ 
    |  154  |  TM mode imaginary Ey                   |
    +-------+-----------------------------------------+ 
    |  155  |  TM mode real Ez                        |
    +-------+-----------------------------------------+ 
    |  156  |  TM mode imaginary Ez                   |
    +-------+-----------------------------------------+ 
    |  161  |  TM mode real Hx                        |
    +-------+-----------------------------------------+ 
    |  162  |  TM mode imaginary Hx                   |
    +-------+-----------------------------------------+ 
    |  163  |  TE mode real Hy                        |
    +-------+-----------------------------------------+ 
    |  164  |  TE mode imaginary Hy                   |
    +-------+-----------------------------------------+ 
    |  165  |  TE mode real Hz                        |
    +-------+-----------------------------------------+ 
    |  166  |  TE mode imaginary Hz                   |
    +-------+-----------------------------------------+ 

MT data types 151–166 are useful for model studies of the raw MT field
behavior; they are scaled relative to a unit magnitude downward
propagating magnetic source field at the model top.

Response File
-------------

The response file is identical to the data file, except that the data
section has two more columns, one for the model response and another for
the weighted residual. The format line is EMResp_2.2 instead of
EMData_2.2.  The responses files are output from MARE2DEM as
``filename.1.resp``, ``filename.2.resp``, ... for each inversion
iteration.

 